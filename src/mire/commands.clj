(ns mire.commands
  (:require [clojure.string :as str]
            [mire.rooms :as rooms]
            [mire.player :as player]))

(defn- move-between-refs
  "Move one instance of obj between from and to. Must call in a transaction."
  [obj from to]
  (alter from disj obj)
  (alter to conj obj))

;; Command functions

(defn- parse-number
  "Reads a number from a string. Returns nil if not a number."
  [s]
  (if (re-find #"^-?\d+\.?\d*$" s)
    (read-string s)))

(defn- add-beans
  "Add %number% beans"
  [number]
  (dosync
    (ref-set player/*beans* (+ @player/*beans* number))))

(defn- reduce-beans
  "Reduce %number% beans"
  [number]
  (dosync
    (ref-set player/*beans* (- @player/*beans* number))))

(defn look
  "Get a description of the surrounding environs and its contents."
  []
  (str (:desc @player/*current-room*)
       "\nExits: " (keys @(:exits @player/*current-room*)) "\n"
       (str "\nBeans in this room: ")
       (str @(player/*current-room* :beans))))

(defn grab
  "Pick something up."
  [thing]
  (dosync
    (cond (integer? (parse-number thing))
          (if (>= @(player/*current-room* :beans) (parse-number thing))
            (do
              (add-beans (parse-number thing))
              (println "You picked up " (parse-number thing) " beans")
              (ref-set (player/*current-room* :beans) (- @(player/*current-room* :beans) (parse-number thing)))
              (str ""))
            (str "In this room " @(player/*current-room* :beans) " beans"))
     :else (if (= :beans (keyword thing))
             (do
               (add-beans @(player/*current-room* :beans))
               (println "You picked up " @(player/*current-room* :beans) " beans")
               (ref-set (player/*current-room* :beans) 0)
               (str ""))))))

(defn discard
  "Put something down that you're carrying."
  [thing]
  (dosync
    (cond (integer? (parse-number thing))
          (if (>= @player/*beans* (parse-number thing))
            (do
              (reduce-beans (parse-number thing))
              (println "You dropped " (parse-number thing) " beans")
              (ref-set (player/*current-room* :beans) (+ @(player/*current-room* :beans) (parse-number thing)))
              (str ""))
            (do
              (str "You have " @player/*beans* " beans")))
    :else (if (= :beans (keyword thing))
            (do
              (println "You dropped " @player/*beans* " beans")
              (ref-set (player/*current-room* :beans) @player/*beans*)
              (reduce-beans @player/*beans*))))))

(defn viewbeans
  "See your beans"
  []
  (str (str/join "\r\n" (map #(str "Beans is " % " .\r\n") [(str @player/*beans*)]))))

(defn move
  "\"♬ We gotta get out of this place... ♪\" Give a direction."
  [direction]
  (dosync
   (let [target-name ((:exits @player/*current-room*) (keyword direction))
         target (@rooms/rooms target-name)]
     (if target
       (do
         (move-between-refs player/*name*
                            (:inhabitants @player/*current-room*)
                            (:inhabitants target))
         (ref-set player/*current-room* target)
         (look))
       "You can't go that way."))))

(defn inventory
  "See what you've got."
  []
  (str "You are carrying:\n"
       (str/join "\n" (seq @player/*inventory*))))

(defn detect
  "If you have the detector, you can see which room an item is in."
  [item]
  (if (@player/*inventory* :detector)
    (if-let [room (first (filter #((:items %) (keyword item))
                                 (vals @rooms/rooms)))]
      (str item " is in " (:name room))
      (str item " is not in any room."))
    "You need to be carrying the detector for that."))

(defn say
  "Say something out loud so everyone in the room can hear."
  [& words]
  (let [message (str/join " " words)]
    (doseq [inhabitant (disj @(:inhabitants @player/*current-room*)
                             player/*name*)]
      (binding [*out* (player/streams inhabitant)]
        (println message)
        (println player/prompt)))
    (str "You said " message)))

(defn help
  "Show available commands and what they do."
  []
  (str/join "\n" (map #(str (key %) ": " (:doc (meta (val %))))
                      (dissoc (ns-publics 'mire.commands)
                              'execute 'commands))))

;; Command data

(def commands {"move" move
               "north" (fn [] (move :north))
               "south" (fn [] (move :south))
               "east" (fn [] (move :east))
               "west" (fn [] (move :west))
               "grab" grab
               "discard" discard
               "inventory" inventory
               "detect" detect
               "look" look
               "say" say
               "viewbeans" viewbeans
               "help" help})

;; Command handling

(defn execute
  "Execute a command that is passed to us."
  [input]
  (try (let [[command & args] (.split input " +")]
         (apply (commands command) args))
       (catch Exception e
         (.printStackTrace e (new java.io.PrintWriter *err*))
         "You can't do that!")))
